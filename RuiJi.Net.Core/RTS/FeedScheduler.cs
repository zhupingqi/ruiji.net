﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuiJi.Net.Core.RTS
{
    /// <summary>
    /// feed monitor scheduler
    /// </summary>
    public class FeedScheduler
    {
        private static IScheduler scheduler;
        private static StdSchedulerFactory factory;

        static FeedScheduler()
        {
            factory = new StdSchedulerFactory();
        }

        /// <summary>
        /// start scheduler
        /// </summary>
        public static async void Start()
        {
            scheduler = await factory.GetScheduler();

            //IJobDetail job = JobBuilder.Create<FeedJob>().Build();

            //if (dic != null)
            //{
            //    foreach (var key in dic.Keys)
            //    {
            //        job.JobDataMap.Add(key, dic[key]);
            //    }
            //}

            //ITrigger trigger = TriggerBuilder.Create().WithCronSchedule(cornExpression).Build();

            //await scheduler.ScheduleJob(job, trigger);
            await scheduler.Start();
        }

        public static async void AddJob(string jobKey, string[] cornExpressions, Dictionary<string, object> dic = null)
        {
            scheduler = await factory.GetScheduler();

            var exists = await scheduler.CheckExists(new JobKey(jobKey));

            if (!exists)
            {
                IJobDetail job = JobBuilder.Create<FeedJob>().WithIdentity(jobKey).Build();

                if (dic != null)
                {
                    foreach (var key in dic.Keys)
                    {
                        job.JobDataMap.Add(key, dic[key]);
                    }
                }

                foreach (var cornExpression in cornExpressions)
                {
                    ITrigger trigger = TriggerBuilder.Create().WithCronSchedule(cornExpression).WithIdentity(jobKey).Build();
                    await scheduler.ScheduleJob(job, trigger);
                }
            }
        }

        public static void AddJob(string jobKey, string cornExpression, Dictionary<string, object> dic = null)
        {
            AddJob(jobKey, new string[] { cornExpression }, dic);
        }

        public static async void DeleteJob(string jobKey)
        {
            var job = new JobKey(jobKey);

            await scheduler.DeleteJob(job);
        }

        /// <summary>
        /// stop scheduler
        /// </summary>
        public static async void Stop()
        {
            await scheduler.Shutdown(false);
        }
    }
}
